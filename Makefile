kiga-all-but-docroot:
	ansible-playbook -i kiga, site.yml --skip-tags=set_docroot

kiga:
	ansible-playbook -i kiga, site.yml

kiga-test:
	ansible-playbook -i kiga-test, site.yml

reqs:
	ansible-galaxy install -r requirements.yml

force-reqs:
	ansible-galaxy install -r requirements.yml  --force
