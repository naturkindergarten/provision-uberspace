# kiga provision Uberspace

Ansible playbook for kiga Uberspace server provisioning.

## Prerequisites

Provide an ssh configuration named `kiga` in your `.ssh/config`. Setup your
U7 Uberspace for this ssh connection by adding your public key as ssh-key 
in the dashboard.

Run `make reqs` to install the requirements or `make force-reqs` to upgrade the requirements.

## Production run

```shell script
make kiga
```

## Test run

Requires a U7 Uberspace and an `.ssh/config` setup for kiga-test 

```shell script
make kiga-test
```

## How to setup the mail client

### IMAP
Server: read.uberspace.de
Port: 993
Connection Security: SSL/TLS Normal password

### POP3
Server: read.uberspace.de
Port: 995
Connection Security: SSL/TLS Normal password

### SMTP
Server: read.uberspace.de
Port: 587
Connection Security: STARTTLS Normal password

User: info@kiga.uber.space

See also: https://manual.uberspace.de/mail-access.html
